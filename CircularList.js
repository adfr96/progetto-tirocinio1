/**
 *  a simple library to managed a circular list

 */

/**
 * Circle List.
 *
 * @constructor
 */
function CircularList(){
	this.clear();
}

/**
 * Method used to clear the list.
 *
 * @return {undefined}
 */
CircularList.prototype.clear = function() {

	// Properties
	this.head = null;
	this.tail = null;
	this.size = 0;
};
/**
 * Method use to add an item on the head of the list
 * @param  {any}    item - The item to add.
 * @return {undefined}
 */
CircularList.prototype.add = function(item) {
	var node = {item: item, next: null};

	if(!this.head)
	{
		this.head= node;
		this.tail= node;
		this.head.next= node;
	}
	else{
		node.next= this.head;
		this.tail.next= node;
		this.head= node;
	}

	this.size++;
};

/**
 * Method use to add an item to the end of the list
 * 
 * @param  {any}    item - The item to add.
 * @return {undefined}
 */
CircularList.prototype.push = function(item){
	var node = {item: item, next: null};
	if(!this.head){
		this.head= node;
		this.tail= node;
		this.head.next= node;
	}
	else{
		node.next= this.head;
		this.tail.next= node;
		this.tail= node;
	}

	this.size++;
}

/**
 * Method use to find a node that has the key as its item
 * @param {any} item- the item to find
 * @return {node} the node contains the searched item
 */
CircularList.prototype.getNode = function (key){
	var current= this.head;
	var prev = null;
	var found = null;
	while (found==null && prev!=this.tail)
	{
		if(_.isEqual(current.item,key))
		{
			found= current;
		}
		prev = current;
		current = current.next;
	}
	return found;
}

/**
 * Method use to add an item after the key element passed by parameter
 * @param {any} item - the item to add
 * @param {any} key - the item to find
 * @return {undefined}
 */
CircularList.prototype.addAfter = function(key,item) {
	var node = {item: item, next: null};
	if(!this.head){
		this.head= node;
		this.tail= node;
		this.head.next= node;
	}
	else{
		var found = this.getNode(key);
		if(found != null)
		{
			if(found == this.tail)
				this.tail = node;
			node.next = found.next;
			found.next = node;
		}
	}
}

/**
 * Method use to remove an item from the list
 * @param {any} item - the item to remove
 */
CircularList.prototype.remove = function (item){
	/*
	 * se l'ogetto da rimuovere è in testa, sposto direttamente la testa,
	 *  e cambio il riferimento all'ultimo
	 */
	if(_.isEqual(this.head.item,item))
	{
		this.head = this.head.next;
		this.tail.next = this.head;
		return true;
	}
	else
	{
		var prev = this.find(item);
		if(prev != null)
		{
			if(prev.next == this.tail)
				this.tail = prev;
			prev.next = prev.next.next;
			return true;
		}
		return false;
	}
}

/**
 * Method use to find an item in the list, returned the node previus to what we were looking for
 * @param {any} item- the item to find
 * @return {node} found - the node previus to what we were looking for
 */
CircularList.prototype.getPrevNode = function (key){
	var current= this.head;
	var prev = null;
	var found = null;
	while (found==null && prev!=this.tail)
	{
		if(_.isEqual(current.next.item,key))
		{
			found = current;
		}
		prev = current;
		current = current.next;
	}
	return found;
}

/**
* Method use to exstract a sublist between two item
* @param {any} item- the item to find
* @return {node} found - the node previus to what we were looking for
*/

CircularList.prototype.getSubList = function (sourceItem,destItem){
	var subList = new CircularList();
	var current = this.getNode(sourceItem);
	if(current != null)
	{
		var prev = current;
		while(!_.isEqual(prev.item,destItem))
		{
			//console.log("oggetto aggiunto :");console.log(current);
			subList.push(current.item);
			prev = current;
			current = current.next;
		}

	}
	return subList;
}

CircularList.prototype.printList = function(){
	current = this.head;
	prev = null;
	while(prev != this.tail)
	{
		console.log(current);
		prev = current;
		current = current.next;
	}
}