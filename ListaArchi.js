/**
*	una semplice classe per gestire una lista Circolare doppiamente concatenata
*	di oggetti di tipo Arco: {sorgente, destinazione, facciaSx, facciaDx}
*/

/**
*	Lista Archi 
* 	@Costruttore
*/

function ListaArchi(){
	this.clear();
}

/**
 * Metodo usato per pulire la lista e inizializzare gli attributi.
 *
 * @return {undefined}
 */
ListaArchi.prototype.clear = function() {

	// Attributi
	this.head = null;
	this.tail = null;
	this.lenght = 0;
};

/**
* ritorna l'oggetto contenuto nel primo elemento della lista
*/
ListaArchi.prototype.getFirst = function() {
	return this.head.item
};

/**
 * Metodo per aggiungere un arco in testa alla lista
 * @param  {any}    item - The item to add.
 * @return {undefined}
 */
ListaArchi.prototype.add = function(item) {
	var node = {item: item, next: null , prev: null };

	/*
	console.log("aggiunta dell'arco 5 in quetsta lista:");
	console.log(this.printList());
	*/

	if(this.head == null)
	{
		node.prev=node;
		node.next=node;
		this.head = node;
		this.tail = node;
		this.head.next = node;

	}
	else{

		node.next = this.head;
		node.prev = this.tail;
		this.head.prev = node;
		this.tail.next = node;
		this.head = node;
	}

	this.lenght++;
};

/**
 * Metodo per aggiungere un arco in coda alla lista
 * 
 * @param  {any}    item - The item to add.
 * @return {undefined}
 */
ListaArchi.prototype.push = function(item){
	var node = {item: item, next: null, prev: null};
	if(!this.head){
		this.head = node;
		this.tail = node;
		this.head.next = node;
	}
	else{
		node.next = this.head;
		node.prev = this.tail; 
		this.head.prev = node;
		this.tail.next= node;
		this.tail= node;
	}

	this.lenght++;
};

ListaArchi.prototype.toArray = function(){
	var current = this.head;
	var prev = null;
	var arr = Array(0);
	while (prev!=this.tail)
	{
		arr.push(current.item);
		prev = current;
		current = current.next;
	}
	return arr;
}

/**
 * Metodo usato per trovare l'elemento della lista che ha come item, la chiave passata
 * @param {any} key - the item to find
 * @return {elem} the element contains the searched item
 */
ListaArchi.prototype.getElement = function (key){
	var current = this.head;
	var prev = null;
	var found = null;
	while (found==null && prev!=this.tail)
	{
		if(_.isEqual(current.item,key))
		{
			found = current;
		}
		prev = current;
		current = current.next;
	}
	return found;
}

/**
 * Method use to add an item after the key element passed by parameter
 * @param {any} item - the item to add
 * @param {any} key - the item to find
 * @return {undefined}
 */
ListaArchi.prototype.addAfter = function(key,item) {
	var node = {item: item, next: null, prev: null};
	if(!this.head){
		this.head= node;
		this.tail= node;
		this.head.next= node;
		this.lenght++;
	}
	else{
		var found = this.getElement(key);
		if(found != null)
		{
			if(found == this.tail)
				this.tail = node;
			node.next = found.next;
			node.prev = found;
			found.next.prev = node;
			found.next = node;
			this.lenght++;
		}
	}

}

/**
 * metodo per aggiungere un elemento prima di quello con item la chiave passata
 * @param {any} item - the item to add
 * @param {any} key - the item to find
 * @return {undefined}
 */
ListaArchi.prototype.addFirst = function(key,item) {
	var node = {item: item, next: null, prev: null};
	if(!this.head){
		this.head= node;
		this.tail= node;
		this.head.next= node;
		this.lenght++;
	}
	else{
		var found = this.getElement(key);
		if(found != null)
		{
			if(found == this.head)
				this.head = node;
			node.prev = found.prev;
			node.next = found;
			found.prev.next = node;
			found.prev = node;
			this.lenght++;
		}
	}

}


/**
 * Metodo usato per trovare l'oggetto arco con una data destinazione
 * @param {any} dest - destinazione dell'arco da trovare
 * @return {elem} the searched item
 */
ListaArchi.prototype.findByDestinazione = function (dest){
	var current = this.head;
	var prev = null;
	var found = null;
	while (found==null && prev!=this.tail)
	{
		if(dest == current.item.destination.ID)
		{
			found = current.item;
		}
		prev = current;
		current = current.next;
	}
	return found;
};

/**
 * Metodo usato per trovare l'oggetto arco con una data sorgente
 * @param {any} source - sorgente dell'arco da trovare
 * @return {elem} the searched item
 */
ListaArchi.prototype.findBySorgente = function (source){
	var current = this.head;
	var prev = null;
	var found = null;
	while (found==null && prev!=this.tail)
	{
		if(source == current.item.source.ID)
		{
			found = current.item;
		}
		prev = current;
		current = current.next;
	}
	return found;
};

/**
 * Metodo usato per trovare l'oggetto arco che abbia il nodo dato come sorgente o come destinazione
 * @param {any} nodeID - id del nodo da cercare
 * @return {elem} the searched item
 */
ListaArchi.prototype.findBySorgenteOrDestinazione = function (nodeID){
	var current = this.head;
	var prev = null;
	var found = null;
	while (found==null && prev!=this.tail)
	{
		if((nodeID == current.item.source.ID) || (nodeID == current.item.destination.ID) )
		{
			found = current.item;
		}
		prev = current;
		current = current.next;
	}
	return found;
}

ListaArchi.prototype.findBySorgenteOrDestinazioneBis = function (nodeID , nodeID2){
	var current = this.head;
	var prev = null;
	var found = null;
	while (found==null && prev!=this.tail)
	{
		if((nodeID == current.item.source.ID) || (nodeID == current.item.destination.ID)
			|| (nodeID2 == current.item.source.ID) || (nodeID2 == current.item.destination.ID) )
		{
			found = current.item;
		}
		prev = current;
		current = current.next;
	}
	return found;
}
/**
 * Metodo usato per trovare l'oggetto arco che abbia il nodo dato come sorgente o come destinazione
 * @param {any} nodeID - id del nodo da cercare
 * @return {elem} the searched item
 */
ListaArchi.prototype.findBySorgenteOrDestinazioneFromElement = function (startItem,nodeID){
	var startElement = this.getElement(startItem);
	var current = startElement;
	var prev = null;
	var found = null;
	while (found==null && prev!=startElement.prev)
	{
		if((nodeID == current.item.source.ID) || (nodeID == current.item.destination.ID))
		{
			found = current.item;
		}
		prev = current;
		current = current.next;
	}
	return found;
}

/**
 * Metodo per rimuovere un elemento dalla lista
 * @param {any} item - the item to remove
 */
ListaArchi.prototype.remove = function (item){
	/*
	 * se l'ogetto da rimuovere è in testa, sposto direttamente la testa,
	 *  e cambio il riferimento all'ultimo
	 */
	 
	if(_.isEqual(this.head.item,item))
	{
		this.head = this.head.next;
		this.tail.next = this.head;
		return true;
	}
	else
	{
		var found = this.getElement(item);
		if(found != null)
		{
			if(found == this.head)
			{
				this.head = found.next;
				this.tail.next = found.next;
			}
			if(found == this.tail)
			{
				this.head.prev = found.prev;
				this.tail = found.prev;
			}
			found.prev.next = found.next
			found.next.prev = found.prev
			return true;
		}
		return false;
	}
	
}

/**
*	Metodo per sostituire l'item di un elemento, senza cambiare la sua posizione nella lista
*	@param {any} key - l'item da sostituire
*	@param {any} item - l'item da inserire
*/

ListaArchi.prototype.sostituisciElement = function(key,item) {
	var found = this.getElement(key);
	if(found != null)
	{
		var oldItem = found.item;
		found.item = item;
		return oldItem;
	}
	return null;
};

/**
*	Metodo per estrarre una sottolista, dal nodo contentente start al nodo contenente gol
*/

ListaArchi.prototype.getSubList = function(start, gol) {
	var startElement = this.getElement(start);
	var golElement = this.getElement(gol);
	if((startElement != null) && (golElement != null))
	{
		var subList = new ListaArchi();
		subList.add(startElement.item);
		var current = startElement.next;
		var prev = null;
		while(prev != golElement)
		{
			subList.push(current.item);
			prev = current;
			current = current.next;
		}
		return subList;
	}
	return null;
};

/**
*	Metodo per rimuovere una sottolista, dal nodo contentente start al nodo contenente gol
*/

ListaArchi.prototype.removeSubList = function(start, gol) {
	var startElement = this.getElement(start);
	var golElement = this.getElement(gol);
	if((startElement != null) && (golElement != null))
	{
		this.remove(startElement.item);
		this.lenght--;
		var current = startElement.next;
		var prev = null;
		while(prev != golElement)
		{
			this.remove(current.item);
			this.lenght--;
			prev = current;
			current = current.next;
		}
	}
};

/**
*	Metodo che cambia la faccia destra di tutti gli archi della lista con quella passata came parametro
*/
ListaArchi.prototype.sostiusciFaccia = function(oldFace, newFace) {
	var current = this.head
	var prev = null
	while(prev != this.tail)
	{
		if(_.isEqual(current.item.fdx,oldFace))
		{
			current.item.fdx = newFace;
		}
		if(_.isEqual(current.item.fsx,oldFace))
		{
			current.item.fsx = newFace;
		}
		prev = current;
		current = current.next;
	}	
 
};

ListaArchi.prototype.printList = function(){
	current = this.head;
	prev = null;
	while(prev != this.tail)
	{
		//console.log("item");
		console.log(current.item);
		//console.log("next");
		//console.log(current.next);
		//console.log("prev");
		//console.log(current.prev);
		prev = current;
		current = current.next;
	}
};


ListaArchi.prototype.findBySorgenteOrDestinazioneBis = function (nodeID){
	var current = this.head;
	var prev = null;
	var found = null;
	while (found==null && prev!=this.tail)
	{
		if(((nodeID == current.item.source.ID) || (nodeID == current.item.destination.ID)) &&
			((nodeID == current.next.item.source.ID) || (nodeID == current.next.item.destination.ID)))
		{
			found = current;
		}
		prev = current;
		current = current.next;
	}
	return found.next.item;
};

/**
*	Metodo per rimuovere una sottolista, dal nodo contentente start al nodo contenente gol
*/

ListaArchi.prototype.removeSubListNew = function(startElement, golElement) {

	if((startElement != null) && (golElement != null))
	{
		this.remove(startElement.item);
		this.lenght--;
		var current = startElement.next;
		var prev = null;
		while(prev != golElement)
		{
			this.remove(current.item);
			this.lenght--;
			prev = current;
			current = current.next;
		}
	}
};
