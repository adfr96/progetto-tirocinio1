/**
*	Oggetto che rappresenta un arco del grafo. 
*	Rispetto ad una rappresentazione classica (sorgente, destinazione) qui abbiamo informazioni 
*	sulle facce a cui è connesso l'arco(per poterne sapere la sua posizione rispetto agli altri archi)
*/

/**
 * Arco
 *
 * @constructor
 */
function Arco(ID,sorgente, destinazione, facciaSx, facciaDx){
	this.ID = ID;
	this.source = sorgente;
	this.destination = destinazione;
	this.fsx = facciaSx;
	this.fdx = facciaDx;
}

Arco.prototype.chi_sono = function() {
	return "arco";
};