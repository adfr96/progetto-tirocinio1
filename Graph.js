/**
* ======= IL SENSO DI PERCORRENZA DELLE LISTE CIRCOLARI è ORARIO =======
*
*	Oggetto che rappresenta il grafo in una rappresentazione nodi,archi,facce.
*/
function Graph(){
	this.nodi = new Map();
	this.archi = new Map();
	this.facce = new Map();
	this.array_nodi = new Array(0);
	this.array_archi = new Array(0);
	this.array_facce = new Array(0);
	this.maxIdNodi = 0;
	this.maxIdArchi = 0;
	this.maxIdFacce = 0;
	this.init();
};

/**
*	Metodo per inizializzare il grafo con tre nodi,tre archi e 2 facce, 
*	cioè il grafo minimo in questa rappresentazione 
*/
Graph.prototype.init = function() {
	/* creo e inserisco i 3 nodi */
	var n1 = new Nodo(++this.maxIdNodi);
	this.nodi.set(n1.ID, n1);
	var n2 = new Nodo(++this.maxIdNodi);
	this.nodi.set(n2.ID, n2);
	var n3 = new Nodo(++this.maxIdNodi);
	this.nodi.set(n3.ID, n3);

	/* creo e inserisco le 2 facce*/
	var f1 = new Faccia(++this.maxIdFacce);
	var f2 = new Faccia(++this.maxIdFacce)
	this.facce.set(f1.ID, f1);
	this.facce.set(f2.ID, f2);

	/* creo i 3 archi*/
	var a1 = new Arco(++this.maxIdArchi,n1, n2, f1, f2);
	var a2 = new Arco(++this.maxIdArchi,n2, n3, f1, f2);
	var a3 = new Arco(++this.maxIdArchi,n3, n1, f1, f2);
	
	/* connetto gli archi ai nodi */
	n1.addArco(a1);
	n2.addArco(a1);
	n2.addArcoAfter(a1, a2);
	n3.addArco(a2);
	n3.addArcoAfter(a2, a3);
	n1.addArcoAfter(a1, a3);

	/* connetto gli archi alle facce*/
	f1.addArco(a1);
	f1.addArcoAfter(a1, a3);
	f1.addArcoAfter(a3, a2);
	f2.addArco(a1);
	f2.addArcoAfter(a1, a2);
	f2.addArcoAfter(a2, a3);

	/*inserisco gli archi*/
	this.archi.set(a1.ID, a1);
	this.archi.set(a2.ID, a2);
	this.archi.set(a3.ID, a3);

	this.array_nodi.push(n1);
	this.array_nodi.push(n2);
	this.array_nodi.push(n3);
	
	this.array_archi.push(a1);
	this.array_archi.push(a2);
	this.array_archi.push(a3);

	this.array_facce.push(f1);
	this.array_facce.push(f2);

};
function printArchiList(value, key , map){
		console.log("ID:"+key+" lista archi:");
		value.archi.printList();
	};
Graph.prototype.addNodoInArco = function(source , destination) {
	/* 
		- creare il nuovo nodo
		- indiduare l'arco da splittare
		- creare nuovo arco
		- modificare vecchio arco
		- sostiruire arco nel nodo destinazione
		- aggiungere archi al nuovo nodo
		- aggiugere nuovo arco alle facce coinvolte
	
		riciclo il vecchio arco lasciandolo connesso al nodo di partenza
	*/

	var sourceNode = this.nodi.get(source);
	var destinationNode = this.nodi.get(destination);
	if(sourceNode == destinationNode)
		return null;
	var oldArc = sourceNode.findArcoByDestinazione(destination);

	if(oldArc == null)
		return null;

	var facciaSx = oldArc.fsx;
	var facciaDx = oldArc.fdx;
	var newNodo = new Nodo(++this.maxIdNodi);
	var newArc = new Arco(++this.maxIdArchi, newNodo , destinationNode, facciaSx , facciaDx);

	oldArc.destination = newNodo;
	destinationNode.sostituisciArco(oldArc, newArc);
	
	newNodo.addArco(newArc);
	newNodo.addArco(oldArc);

	facciaSx.addArcoFirst(oldArc, newArc);
	facciaDx.addArcoAfter(oldArc, newArc);

	this.nodi.set(newNodo.ID,newNodo);
	this.archi.set(newArc.ID,newArc);

	this.array_nodi.push(newNodo);
	this.array_archi.push(newArc);

	return newNodo;
};

Graph.prototype.addArcoInFaccia = function(source, destination, faceID){
	var sourceNode = this.nodi.get(source);
	var destinationNode = this.nodi.get(destination);
	var faccia = this.facce.get(faceID);
	var subList;

	subList = faccia.getSubListaArchi(source,destination);
	if(subList == null)
		return;
	
	var newFaccia = new Faccia(++this.maxIdFacce);
	var newArc = new Arco(++this.maxIdArchi, sourceNode, destinationNode, newFaccia,faccia );
	newFaccia.setListaArchiFromFace(subList,faccia);
	faccia.addArcoAfter(subList.tail.item,newArc);
	faccia.removeSubListaArchi(source, destination);
	
	sourceNode.addArcoAfter(subList.getFirst(), newArc);
	destinationNode.addArcoFirst(subList.tail.item, newArc);	

	newFaccia.addArco(newArc);	

	this.facce.set(newFaccia.ID,newFaccia);
	this.archi.set(newArc.ID, newArc);

	this.array_archi.push(newArc);
	this.array_facce.push(newFaccia);
	return newArc;
};

Graph.prototype.test = function(source, destination, faceID) {
	var faccia = this.facce.get(faceID);
	console.log("SubListaArchi:");console.log(faccia.getSubListaArchi(source,destination));
};
