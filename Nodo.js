/**
*	Oggetto Nodo che rappresenta un nodo del grafo, avrà quindi un Id e una lista di archi connessi ad esso
*/

/**
 * Nodo
 *
 * @constructor
 */
function Nodo(ID){
	this.ID = ID;
	this.archi = new ListaArchi(); //lista circolare degli archi connessi al nodo
}

Nodo.prototype.chi_sono = function() {
	return "nodo";
};
/**
*	Metodo per aggiungere un arco in testa alla lista di archi del nodo
*	@param newArco - nuovo arco da aggiungere
*/
Nodo.prototype.addArco = function (newArco){
	this.archi.add(newArco);
}

/**
*	Metodo per aggiungere un arco dopo un arco dato 
*	@param key - arco chiave
*	@param newArco - nuovo arco da aggiungere
*/
Nodo.prototype.addArcoAfter = function(key,newArco) {
	/*
	console.log("addArco after: \n key:");
	console.log(key);
	console.log("new arco");
	console.log(newArco);
	*/
	this.archi.addAfter(key, newArco);
};

Nodo.prototype.addArcoFirst = function(key,newArco) {
	this.archi.addFirst(key, newArco);
};

/**
*	Metodo per trovare un arco che "parte" da this e ha una certa destinazione
*	@param {any} destination - nodo destinazione 
*/
Nodo.prototype.findArcoByDestinazione = function(destination) {
	return this.archi.findByDestinazione(destination);
};

/**
*	Metodo per sostituire un arco senza cambiarne l'ordine 
*	@param oldArco - arco da sostituire
*	@param newArco - nuovo arco da aggiungere
*/
Nodo.prototype.sostituisciArco = function(oldArco, newArco) {
	return this.archi.sostituisciElement(oldArco, newArco);
};
