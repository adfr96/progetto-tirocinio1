/**
 *	Oggetto che rappresenta una faccia del grafo (regione di spazio "vuota" chiusa da archi).
 *	Ogni oggetto ha un id e una lista Circolare di Archi ad essa associati, 
 *	che rappresenta gli archi che "si affacciano" su quella faccia
 */

/**
 * Faccia
 *
 * @constructor
 */
function Faccia(ID){
	/*
	* la lista sarà formata dagli archi rappresentati nella forma {source, destination}
	*/
	this.ID = ID;
	this.archi = new ListaArchi();
}

Faccia.prototype.chi_sono = function() {
	return "faccia";
};
/**
*	Metodo set per cambiare la lista degli archi della faccia
*/
Faccia.prototype.setListaArchiFromFace = function(listaArchi,oldFace) {
	this.archi = listaArchi;
	/*nel riciclare le facce devo modificare gli archi che erano attacati ad un'altra faccia
	 quindi sostituire la vecchia faccia(sia che sia a destra che a sinistra) con quella attuale*/
	this.archi.sostiusciFaccia(oldFace,this);
};


/**
* Method to add an acrh to the face, at the top of the list
* @param arco - arco da aggiungere
*/
Faccia.prototype.addArco = function(arco){
	this.archi.add(arco);
}

/**
* Method to add an acrh to the face, after an alredy existing arch
* @param arch -  the arch that alredy exist
* @param source - the source of new arch
*/
Faccia.prototype.addArcoAfter = function(key,newArco) {
	this.archi.addAfter(key, newArco);
};

/**
* Method to add an acrh to the face, first an alredy existing arch
* @param arch -  the arch that alredy exist
* @param source - the source of new arch
* @param destination - the destination of new arch
*/
Faccia.prototype.addArcoFirst = function(key, newArco) {
	this.archi.addFirst(key, newArco);
};


/**
*	Metodo per trovare un arco nelle lista che abbia o come sorgente o come destinazione, 
*	il nodo con ID passato
*/
Faccia.prototype.findBySorgenteOrDestinazione = function(nodeID) {
	return this.archi.findBySorgenteOrDestinazione(nodeID);
};


Faccia.prototype.getSubListaArchi = function(nodoIDStart, nodoIDGol) {
	var arcoStart = this.archi.findBySorgenteOrDestinazioneBis(nodoIDStart);
	var arcoGol = this.archi.findBySorgenteOrDestinazioneFromElement(arcoStart, nodoIDGol);
	if(arcoStart == null || arcoGol == null || arcoStart==arcoGol)
		return;
	return this.archi.getSubList(arcoStart, arcoGol);
};

Faccia.prototype.removeSubListaArchi = function(nodoIDStart, nodoIDGol) {
	var arcoStart = this.archi.findBySorgenteOrDestinazioneBis(nodoIDStart);
	var arcoGol = this.archi.findBySorgenteOrDestinazioneFromElement(arcoStart, nodoIDGol);
	if(arcoStart == null || arcoGol == null || arcoStart==arcoGol)
		return;
	return this.archi.removeSubList(arcoStart, arcoGol);
};
